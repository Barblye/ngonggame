import java.io.*;
import java.util.Random;
public class Monster{
    protected int hp, maxHp, level, dmg, exp; //declared attributes
    protected String name, image;


    public Monster(String name) //defined attributes
    {
        level = 1;
        hp = 100;
        maxHp = 100;
        exp = 0;
        dmg = 0;
        this.name = name;

    }

    public int getMaxHp(){
        return maxHp;
    }

    public int getHp(){
        return hp;
    }

    public int getLevel(){
        return level;
    }

    public int getExp(){
        return exp;
    }

    public String getName(){
        return name;
    }

    public int getDmg(){
        return dmg;
    }

    public String getImage(){
        return image;
    }

    public void takingDmg(int dmg){
        hp -= dmg;
        if(hp < 0){
            hp = 0;
        }
    }
}