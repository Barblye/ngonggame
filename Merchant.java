import java.io.*;
import java.util.*;
public class Merchant extends Novice{
    public Merchant(Novice lastNovice){
        super.setJob("Merchant");
        super.setAll(lastNovice.showHp(), lastNovice.showLevel(), lastNovice.showMoney(), lastNovice.bag);
    }

    public void attack(){ 
        super.setExp(5);
        super.setHp(-5);
        if(super.showExp() == (super.showLevel()*100))
        {
            super.setLevel(1);
            super.setMoney(1000);
            super.setExp(-100);
        }
        skillPayforGod();
    }
    private void skillPayforGod(){
        System.out.println("God listened!!! ");
        super.setMoney(-200);
        super.setHp(200);
    }


}