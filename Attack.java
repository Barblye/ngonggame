import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Attack
 */
public class Attack extends JFrame{
    Monster mons;
    public Attack(Monster monster,Novice noviceN){
        
        super("Ragnarok");
        Novice novice = noviceN;
        mons = monster;
        Container c = getContentPane();
        c.setLayout(new GridLayout(3,1));
        ImageIcon novicePic = new ImageIcon(novice.getImage());
        ImageIcon monsterPic = new ImageIcon(monster.getImage());
        JLabel noviceImage = new JLabel(novicePic);
        JLabel monsterImage = new JLabel(monsterPic);

        JPanel g1 = new JPanel();
        g1.setLayout(new FlowLayout());
        JLabel gameAttack = new JLabel("Attack Now!");
        g1.add(gameAttack);
        
        JPanel g2 = new JPanel();
        g2.setLayout(new GridLayout(1,4));
        JPanel monStatus = new JPanel();
        monStatus.setLayout(new BoxLayout(monStatus, BoxLayout.Y_AXIS));
        JLabel monsName = new JLabel("Lunatic");
        JLabel monsLevel = new JLabel("Level: " + mons.getLevel());
        JLabel monsHp = new JLabel("Hp: "+ mons.getHp());
        monStatus.add(monsName);
        monStatus.add(monsLevel);
        monStatus.add(monsHp);

        JPanel picMons = new JPanel();
        picMons.setLayout(new FlowLayout());
        picMons.add(monsterImage);

        JPanel picNovice = new JPanel();
        picNovice.setLayout(new FlowLayout());
        picNovice.add(noviceImage);

        JPanel noviceStatus = new JPanel();
        noviceStatus.setLayout(new BoxLayout(noviceStatus, BoxLayout.Y_AXIS));
        JLabel noviceName = new JLabel("Name: " + novice.getName());
        JLabel noviceSex = new JLabel("Character: " + novice.getSex());
        JLabel noviceLevel = new JLabel("Level: " + novice.getLevel());
        JLabel noviceExp = new JLabel("Exp: " + novice.getExp());
        JLabel noviceHp = new JLabel("Hp: " + novice.getHp());
        noviceStatus.add(noviceName);
        noviceStatus.add(noviceSex);
        noviceStatus.add(noviceLevel);
        noviceStatus.add(noviceExp);
        noviceStatus.add(noviceHp);

        g2.add(monStatus);
        g2.add(picMons);
        g2.add(picNovice);
        g2.add(noviceStatus);

        JPanel g3 = new JPanel();
        g3.setLayout(new FlowLayout());
        JButton setAttack = new JButton("ATTACK");
        setAttack.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                novice.takingDmg(mons.getDmg());
                mons.takingDmg(novice.getAttack());
                noviceHp.setText("Hp: " + novice.getHp());
                monsHp.setText("Hp: "+ mons.getHp());

                if(mons.getHp() <= 0){
                    novice.getExpUp(mons.getExp());
                    noviceExp.setText("Exp: " + novice.getExp());
                    mons = new Lunatic();
                }

                if(novice.getExp() >= 100){
                    novice.setExp();
                    noviceExp.setText("Exp: " + novice.getExp());
                    novice.getLevelUp();
                    noviceLevel.setText("Level: " + novice.getLevel());
                }

            }
        });
        
        g3.add(setAttack);
        c.add(g1);
        c.add(g2);
        c.add(g3);
        setVisible(true);
        pack();
    }
}