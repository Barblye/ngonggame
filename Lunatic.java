import java.io.*;
import java.util.*;

public class Lunatic extends Monster{

    public Lunatic(){
        super("Lunatic");
        hp = 10;
        maxHp = 10;
        level = 1;
        dmg = 1;
        exp = 50;
        image = "lunaticvel1.gif";
    }
}