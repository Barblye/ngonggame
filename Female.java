/**
 * Female
 */
import java.io.*;
import java.util.*;
public class Female extends Novice{
    public Female(String name){
        super(name);
        sex = "Female";
        image = "Female.gif";
    }
}