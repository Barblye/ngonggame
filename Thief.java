import java.io.*;
import java.util.*;
public class Thief extends Novice{
    public Thief(Novice lastNovice){
        super.setJob("Thief");
        super.setAll(lastNovice.showHp(), lastNovice.showLevel(), lastNovice.showMoney(), lastNovice.bag);
    }

    public void attack(){ 
        super.setExp(5);
        super.setHp(-5);
        if(super.showExp() == (super.showLevel()*100))
        {
            super.setLevel(1);
            super.setMoney(1000);
            super.setExp(-100);
        }
        skillMoneyAttack();
    }
    private void skillMoneyAttack(){
        System.out.println("Attack!!!! ");
        super.setMoney(-200);
        super.setExp(100);
    }


}