/**
 * male
 */
import java.io.*;
import java.util.*;
public class Male extends Novice{
    public Male(String name){
        super(name);
        sex = "Male";
        image = "Male.gif";
    }
}