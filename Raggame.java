import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * raggame
 */
public class Raggame extends JFrame {

    public Raggame (){
        super("Ragnarok");
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        JPanel n = new JPanel();
        n.setLayout(new FlowLayout());
        ImageIcon classic = new ImageIcon("classic.png");
        ImageIcon ragnarok = new ImageIcon("ragnarok.png");
        JLabel hImage = new JLabel(ragnarok);
        JLabel cImage = new JLabel(classic);
        n.add(hImage);
        JPanel right = new JPanel();
        right.setLayout(new FlowLayout());
        right.add(cImage);
        JPanel left = new JPanel();
        left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
        JPanel name = new JPanel();
        name.setLayout(new FlowLayout());
        JLabel textName = new JLabel("Enter Your Name ");
        JTextField textField = new JTextField(20);
        name.add(textName);
        name.add(textField);

        JPanel character = new JPanel();
        character.setLayout(new FlowLayout());
        JLabel textName2 = new JLabel("Choose Character ");
        character.add(textName2);


        JPanel sex = new JPanel();
        sex.setLayout(new GridLayout(1,2));
        JButton setMale = new JButton("Male");
        JPanel buttonMale = new JPanel();
        buttonMale.add(setMale); 
        JButton setFemale = new JButton("Female");
        JPanel buttonFemale = new JPanel();
        buttonMale.add(setFemale); 
        sex.add(buttonMale);
        sex.add(buttonFemale);
        sex.setBorder(BorderFactory.createEmptyBorder(0, 150, 200, 0));

        left.setBorder(BorderFactory.createEmptyBorder(0, 150, 0, 0));
        left.add(name); 
        left.add(character);
        left.add(sex);

        c.add(n,BorderLayout.NORTH);
        c.add(left,BorderLayout.WEST);
        c.add(right,BorderLayout.CENTER);
        setSize(500,500);
        setVisible(true);

        setFemale.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                new Attack(new Lunatic(),new Female(textField.getText()));
                dispose();
            }
        });
        setMale.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                new Attack(new Lunatic(),new Male(textField.getText()));
                dispose();
            }
        });
    }
    public static void main(String[] args) {
        new Raggame();
    }
} 