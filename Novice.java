/**
 * Novice
 */
import java.io.*;
import java.util.*;
public class Novice{
    protected int hp, exp, level, maxHp, maxExp, maxLevel, attack;
    protected String name, sex, image;
    public Novice(String name){
        maxHp = 100;
        exp = 0;
        level = 1;
        hp = 100;
        maxLevel = 100;
        maxExp = 100;
        attack = 1;
        sex = "";
        image = "";
        this.name = name;
    } 

    public int getHp(){
        return hp;
    }

    public int getExp(){
        return exp;
    }

    public void setExp(){
        exp = 0;
    }

    public int getLevel(){
        return level;
    }

    public int getAttack(){
        return attack;
    }

    public int getMaxHp(){
        return maxHp;
    }

    public int getMaxExp(){
        return maxExp;
    }

    public int getMaxLevel(){
        return maxLevel;
    }

    public String getSex(){
        return sex;
    }

    public String getImage(){
        return image;
    }

    public String getName(){
        return name;
    }

    public void takingDmg(int dmg){
        hp -= dmg;
        if(hp < 0){
            hp = 0;
        }
    }

    public void getExpUp(int exp){
        this.exp += exp ;
    }

    public void getLevelUp(){
        this.level += 1;
    }
}